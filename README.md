# Sketch
Sketch libraries for wireframes and mockups.

### Wireframes.sketch

- Buttons
- Form fields
- Tags
- Components
- More to come…

### Fontawesome.sketch

The full Font Awesome 4.7 set as Sketch symbols, in both text and outlines.

Font Awesome is made by Dave Gandy - http://fontawesome.io.

## About Spatie
Spatie is a webdesign agency based in Antwerp, Belgium. You'll find an overview of all our open source projects [on our website](https://spatie.be/opensource).

## License
The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

